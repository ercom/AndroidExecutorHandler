package com.ercom.android.utils.handlerexecutor

import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import androidx.test.runner.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun mainThreadExecutorInMainThread() {
        val sync = CountDownLatch(1)
        var result = false

        HandlerExecutor.mainThreadHandlerExecutor.execute({
            assertTrue("mainThreadHandlerExecutor does not run in main thread", Looper.getMainLooper() == Looper.myLooper())
            sync.countDown()
        })

        sync.await()
    }

    @Test
    fun mainThreadExecutorIsAsynchrone() {
        val sync = CountDownLatch(1)
        var result = false

        HandlerExecutor.mainThreadHandlerExecutor.execute({
            Thread.sleep(100)
            sync.countDown()
        })

        assertTrue("Thread finish before execute() continue", sync.count == 1L)
        sync.await(1000, TimeUnit.MILLISECONDS)
        assertTrue("Thread finish before execute() continue", sync.count == 0L)

    }

    @Test
    fun mainThreadExecutorSequential() {
        val count = CountDownLatch(10)
        var result = false

        for (i in 10L downTo 0L) {
            HandlerExecutor.mainThreadHandlerExecutor.execute({
                assertEquals("Execution order not respected", i, count.count)
                Thread.sleep(100)
                assertEquals("Another thread appears to be executed in parallel", i, count.count)

                count.countDown()
            })
        }

        // No countdown called because the first thread, if it was started, if blocked in a sleep
        assertTrue(count.count == 10L)
        count.await(10000, TimeUnit.MILLISECONDS)
        assertTrue(count.count == 0L)
    }

    @Test
    fun handlerThread() {
        val threadName = "testHandlerExecutor"
        val ht = HandlerThread(threadName)

        ht.start()

        val handler = Handler(ht.looper)

        val he = HandlerExecutor(handler)

        val sync = CountDownLatch(1)
        he.execute({
            assertEquals("Execution is not in the correct thread", Looper.myLooper().thread.name, threadName)
            sync.countDown()
        })

        assertTrue(sync.await(100, TimeUnit.MILLISECONDS))
    }
}

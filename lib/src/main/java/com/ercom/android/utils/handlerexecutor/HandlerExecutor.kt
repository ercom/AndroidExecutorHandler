package com.ercom.android.utils.handlerexecutor

import java.util.concurrent.Executor
import android.os.Handler
import android.os.Looper

// Source : https://android.googlesource.com/platform/packages/apps/Camera2/+/master/src/com/android/camera/async/HandlerExecutor.java
class HandlerExecutor(private val mHandler: Handler) : Executor {
    override fun execute(runnable: Runnable) {
        mHandler.post(runnable)
    }

    companion object {
        val mainThreadHandlerExecutor = HandlerExecutor(Handler(Looper.getMainLooper()))
    }
}

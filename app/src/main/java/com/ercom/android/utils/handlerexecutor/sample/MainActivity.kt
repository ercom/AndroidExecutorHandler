package com.ercom.android.utils.handlerexecutor.sample

import android.content.Context
import androidx.databinding.BaseObservable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import android.view.LayoutInflater
import android.view.ViewGroup
import com.ercom.android.utils.handlerexecutor.HandlerExecutor
import com.ercom.android.utils.handlerexecutor.sample.databinding.JobviewBinding
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.ThreadLocalRandom


class MainActivity : AppCompatActivity() {

    var index = 0
    val adapter = JobViewAdapter(this)
    fun createJob() : Job {
        val job = Job(adapter, index)
        jobNumber.text = index++.toString()
        listJob.add(0, job)
        adapter.notifyItemInserted(0)
        return job
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        jobNumber.text = index.toString()
        listViewJobs.adapter = adapter
        listViewJobs.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)

        val handlerThread1 = HandlerThread("Thread1")
        val handlerThread2 = HandlerThread("Thread2")

        handlerThread1.start()
        handlerThread2.start()

        val executor1 = HandlerExecutor(Handler(handlerThread1.looper))
        val executor2 = HandlerExecutor(Handler(handlerThread2.looper))

        btnMainThread.setOnClickListener {
            HandlerExecutor.mainThreadHandlerExecutor.execute(createJob())
        }
        btnThread1.setOnClickListener {
            executor1.execute(createJob())
        }
        btnThread2.setOnClickListener {
            executor2.execute(createJob())
        }
    }
}

val listJob = mutableListOf<Job>()
val mainThread = Handler(Looper.getMainLooper())

class Job (val adapter: JobViewAdapter, val jobNumber : Int) : Runnable, BaseObservable() {
    enum class State {
        PLANNED,
        RUNNING,
        FINISH
    }

    var state : State = State.PLANNED
    var currentThreadName = ""
    override fun run() {
        state = State.RUNNING
        currentThreadName = Thread.currentThread().name
        notifyChange()
        Thread.sleep(ThreadLocalRandom.current().nextLong(1000, 3000))
        state = State.FINISH
        currentThreadName = ""
        notifyChange()
        mainThread.postDelayed({
            // Remove job entry when it is finished, 100ms later
            val index = listJob.indexOf(this)
            listJob.removeAt(index)
            adapter.notifyItemRemoved(index)
        }, 100)
    }
}

class JobViewHolder(val jobData: JobviewBinding) : androidx.recyclerview.widget.RecyclerView.ViewHolder(jobData.root) {
    private var job : Job? = null
    fun bind(boundJob: Job) {
        if (job != null) {
            unbind()
        }
        job = boundJob
        jobData.job = job
    }

    fun unbind() {
        jobData.unbind()
        job = null
    }
}

class JobViewAdapter(val context: Context) : androidx.recyclerview.widget.RecyclerView.Adapter<JobViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobViewHolder {
        return JobViewHolder(JobviewBinding.inflate(LayoutInflater.from(context), parent, false))
    }

    override fun getItemCount(): Int {
        return listJob.size
    }

    override fun onBindViewHolder(holder: JobViewHolder, position: Int) {
        val job = listJob[position]
        holder.bind(job)
    }
}
